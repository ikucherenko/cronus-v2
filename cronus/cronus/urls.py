"""cronus URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import obtain_jwt_token
from rest_framework_swagger.views import get_swagger_view
from v2 import views

schema_view = get_swagger_view(title='Pastebin API')

router = DefaultRouter(trailing_slash=False)
router.register(r'monuments', views.ArchitecturalMonumentViewSet)
router.register(r'blueprints', views.BlueprintView)
router.register(r'users', views.UserViewSet)
router.register(r'getallregions', views.RegionViewSet)

urlpatterns = [
	url(r'^api/', include(router.urls, namespace='api')),
	url(r'^api/current', views.CurrentUserView.as_view(), name="current_user"),
    url(r'^api/admin', admin.site.urls),
    url(r'^api/monuments', include('v2.urls')),
    url(r'^api/login', obtain_jwt_token),
    url(r'^api/docs', schema_view),
]
