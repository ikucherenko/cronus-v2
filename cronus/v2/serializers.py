from rest_framework import serializers
from .models import *
from django.contrib.auth.models import User



'''Address section. This section discribes serializers for location'''
class RegionSerializer(serializers.Serializer):
	"""Serializer for Region"""
	name = serializers.CharField()
	id = serializers.IntegerField(read_only=True)
	class Meta:
		model = Region
		fields = ('id', 'name')


class DistrictSerializer(serializers.ModelSerializer):
	"""Serializer for District"""
	region = RegionSerializer()

	class Meta:
		model = District
		fields = ('id', 'name', 'region')


class CitySerializer(serializers.ModelSerializer):
	"""Serializer for City"""
	district = DistrictSerializer(required=False)
	region = RegionSerializer(required=False)
	#id = serializers.IntegerField(required=False)
	class Meta:
		model = City
		fields = ('id', 'name', 'district', 'region')
		extra_kwargs = {
			"id": {
				"read_only": False,
				"required": False,
			},
		}



'''
Monument section. 
This section describes serializations for monument. 
Monument is main model in structure
'''
class CenturySerializer(serializers.Serializer):
	"""Serializer for Century"""
	name = serializers.CharField(required=False)
	#id = serializers.IntegerField(read_only=True)
	class Meta:
		model = Century
		fields = ('name', 'id')
			

class TypeOfMonumentSerializer(serializers.Serializer):
	"""Serializer for TypeOfMonument"""
	name = serializers.CharField()
	
	class Meta:
		model = TypeOfMonument
		fields = ('id', 'name')
		extra_kwargs = {
			"id": {
				"read_only": False,
				"required": False,
			},
		}
		
			


'''Section for photo galerys'''
class BlueprintSerializer(serializers.ModelSerializer):
	"""Serializer for Blueprints"""
	monument = serializers.PrimaryKeyRelatedField(queryset=ArchitecturalMonument.objects.all())
	
	class Meta:
		model = Blueprint
		fields = ('id', 'descr', 'link', 'monument')
			


class ArchitecturalMonumentSerializer(serializers.ModelSerializer):
	"""Serializer for ArchitecturalMonument"""
	city = CitySerializer()
	century = CenturySerializer()
	type = TypeOfMonumentSerializer( read_only=True)
	blueprints = BlueprintSerializer(many=True, read_only=True)

	class Meta:
		depth = 1
		model = ArchitecturalMonument
		fields = ('id', 'name', 'date', 'descr', 'photo', 'city', 'century', 'type', 'blueprints')


	def create(self, validated_data):
		city_data = validated_data.pop('city')
		century_data = validated_data.pop('century')
		type_data = validated_data.pop('type')
		city = None
		if city_data != None:
			if city_data.get('id') != None:
				city = City.objects.get(pk=city_data.get('id'))
			else:
				city = City.objects.create(**city_data)
				

		monument = ArchitecturalMonument.objects.create(city=city, **validated_data)
		TypeOfMonument.objects.create(**type_data)
		Century.objects.create(**century_data)
		return monument



class NewPhotoSerializer(serializers.ModelSerializer):
	"""Serializer for NewPhoto"""
	monument = serializers.PrimaryKeyRelatedField(queryset=ArchitecturalMonument.objects.all())
	
	class Meta:
		model = NewPhoto
		fields = ('__all__')


class OldPhotoSerializer(serializers.ModelSerializer):
	"""Serializer for OldPhoto"""
	monument = serializers.PrimaryKeyRelatedField(queryset=ArchitecturalMonument.objects.all())
	
	class Meta:
		model = OldPhoto
		fields = ('__all__')


class MosaicSerializer(serializers.ModelSerializer):
	"""Serializer for Mosaic"""
	monument = serializers.PrimaryKeyRelatedField(queryset=ArchitecturalMonument.objects.all())
	
	class Meta:
		model = Mosaic
		fields = ('__all__')


class ExternalDecorationSerializer(serializers.ModelSerializer):
	"""Serializer for ExternalDecoration"""
	monument = serializers.PrimaryKeyRelatedField(queryset=ArchitecturalMonument.objects.all())
	
	class Meta:
		model = ExternalDecoration
		fields = ('__all__')


class FloorTileSerializer(serializers.ModelSerializer):
	"""Serializer for FloorTile"""
	monument = serializers.PrimaryKeyRelatedField(queryset=ArchitecturalMonument.objects.all())
	
	class Meta:
		model = FloorTile
		fields = ('__all__')


class FrescoSerializer(serializers.ModelSerializer):
	"""Serializer for Fresco"""
	monument = serializers.PrimaryKeyRelatedField(queryset=ArchitecturalMonument.objects.all())
	
	class Meta:
		model = Fresco
		fields = ('__all__')

class OtherSerializer(serializers.ModelSerializer):
	"""Serializer for Other"""
	monument = serializers.PrimaryKeyRelatedField(queryset=ArchitecturalMonument.objects.all())
	
	class Meta:
		model = Other
		fields = ('__all__')		


class PirofillitSerializer(serializers.ModelSerializer):
	"""Serializer for Pirofillit"""
	monument = serializers.PrimaryKeyRelatedField(queryset=ArchitecturalMonument.objects.all())
	
	class Meta:
		model = Pirofillit
		fields = ('__all__')

class WsDetailSerializer(serializers.ModelSerializer):
	"""Serializer for WsDetails"""
	monument = serializers.PrimaryKeyRelatedField(queryset=ArchitecturalMonument.objects.all())
	
	class Meta:
		model = WsDetail
		fields = ('__all__')



'''Other parts'''
class BookSerializer(serializers.ModelSerializer):
	"""docstring for BookSerializer"""
	monument = serializers.PrimaryKeyRelatedField(queryset=ArchitecturalMonument.objects.all())

	class Meta:
		model = Book
		fields = ('__all__')


class DetailedDescriptionSerializer(serializers.ModelSerializer):
	"""docstring for DetailedDescription"""
	monument = serializers.PrimaryKeyRelatedField(queryset=ArchitecturalMonument.objects.all())

	class Meta:
		model = DetailedDescription
		fields = ('__all__')
		

class ReconstructionSerializer(serializers.ModelSerializer):
	"""docstring for Reconstrction"""
	monument = serializers.PrimaryKeyRelatedField(queryset=ArchitecturalMonument.objects.all())
	century = serializers.PrimaryKeyRelatedField(queryset=Century.objects.all())
	
	class Meta:
		model = Reconstruction
		fields = ('__all__')


class ResearchingHistorySerializer(serializers.ModelSerializer):
	"""docstring for ResearchingHistory"""
	monument = serializers.PrimaryKeyRelatedField(queryset=ArchitecturalMonument.objects.all())

	class Meta:
		model = ResearchingHistory
		fields = ('__all__')


class GeoCoordinateSerializer(serializers.ModelSerializer):
	"""docstring for GeoCoordinate"""
	monument = serializers.PrimaryKeyRelatedField(queryset=ArchitecturalMonument.objects.all())

	class Meta:
		model = GeoCoordinate
		fields = ('__all__')			
		


"""Serializer for model of user"""	
class UserSerializer(serializers.ModelSerializer):
	"""Serializer for user model"""
	class Meta:
		model = User
		fields = ('username', 'first_name', 'last_name', 'email', 'is_staff', 'is_active')
					
