# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-23 15:16
from __future__ import unicode_literals

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('v2', '0023_auto_20170623_1510'),
    ]

    operations = [
        migrations.AlterField(
            model_name='architecturalmonument',
            name='id',
            field=models.UUIDField(blank=True, default=uuid.uuid4, editable=False, primary_key=True, serialize=False),
        ),
    ]
