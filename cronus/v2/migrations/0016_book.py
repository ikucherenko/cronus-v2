# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-06 15:00
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('v2', '0015_wsdetail'),
    ]

    operations = [
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('publisher', models.CharField(max_length=100)),
                ('author', models.CharField(max_length=100)),
                ('descr', models.CharField(max_length=1000)),
                ('monument', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='v2.ArchitecturalMonument')),
            ],
        ),
    ]
