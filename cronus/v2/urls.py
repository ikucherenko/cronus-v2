from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from . import views


app_name = 'v2'
router = DefaultRouter(trailing_slash=False)
router.register(r'blueprints', views.BlueprintViewSet, 'blueprints-by-monument')

urlpatterns = [
	url(r'^(?P<pk>[0-9]+)/', include(router.urls, namespace='api')),

]