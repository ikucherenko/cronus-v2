from django.shortcuts import render, get_object_or_404
from rest_framework import viewsets, permissions, views
from rest_framework.response import Response
from .models import *
from .serializers import *
from django.contrib.auth.models import User


#API`s for each model
class ArchitecturalMonumentViewSet(viewsets.ModelViewSet):
	"""View for monument"""
	#permission_classes = (permissions.IsAuthenticated,)
	permission_classes = (permissions.AllowAny,)
	serializer_class = ArchitecturalMonumentSerializer
	queryset = ArchitecturalMonument.objects.all()


class BlueprintView(viewsets.ModelViewSet):
	"""View for blueprints"""
	serializer_class = BlueprintSerializer
	permission_classes = (permissions.AllowAny,)
	queryset = Blueprint.objects.all()


#API`s for model by id of monument
class BlueprintViewSet(viewsets.ModelViewSet):
	"""Blueprint by monument id"""
	serializer_class = BlueprintSerializer
	# permission_classes = (permissions.IsAdminUser,)
	permission_classes = (permissions.AllowAny,)
	
	def get_queryset(self):
		mon = ArchitecturalMonument.objects.get(pk=self.kwargs.get('pk'))
		return mon.blueprints.all()

class RegionViewSet(viewsets.ModelViewSet):
	"""View for regions"""
	serializer_class = RegionSerializer
	queryset = Region.objects.all()

		


#API`s for authentication models
class UserViewSet(viewsets.ModelViewSet):
	"""View for users"""
	queryset = User.objects.all()
	serializer_class = UserSerializer
	#permission_classes = (permissions.IsAuthenticated,)
	permission_classes = (permissions.AllowAny,)

class CurrentUserView(views.APIView):
	"""View for current (actualy logged in) user"""
	def get(self, request):
		serializer = UserSerializer(request.user)
		return Response(serializer.data)