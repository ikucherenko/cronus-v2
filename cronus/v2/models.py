from django.db import models

'''Address section. Structure of model for location '''

class Region(models.Model):
	"""Model of Region"""
	name = models.CharField(max_length=200, blank=True, null=True)

	def __str__(self):
		return self.name


class District(models.Model):
	"""Model of District"""
	name = models.CharField(max_length=200, blank=True, null=True)
	region = models.ForeignKey('Region') # Many to one relationship

	def __str__(self):
		return self.name
	
class City(models.Model):
	"""Model of City"""
	name = models.CharField(max_length=200, blank=True, null=True)
	district = models.ForeignKey('District', blank=True, null=True) # Many to one relationship
	region = models.ForeignKey('Region', blank=True, null=True) # Many to one relationship

	def __str__(self):
		return self.name


'''
Monument section. 
This section describes model of monument. 
Monument is main model in structure
'''


class Century(models.Model):
	"""Model of century"""
	name = models.CharField(max_length=5, blank=True, null=True)

	def __str__(self):
		return self.name

class TypeOfMonument(models.Model):
	"""Model of TypeOfMonument"""
	name = models.CharField(max_length=25, blank=True)

	def __str__(self):
		return self.name

class ArchitecturalMonument(models.Model):
	"""Model of ArchitecturalMonument"""
	name = models.CharField(max_length=250)
	date = models.CharField(max_length=20, blank=True)
	descr = models.CharField(max_length=1000, blank=True)
	photo = models.CharField(max_length=300, blank=True)

	city = models.ForeignKey('City', null=True, blank=True) # Many to one relationship
	century = models.ForeignKey('Century', null=True, blank=True) # Many to one relationship
	type = models.ForeignKey('TypeOfMonument', null=True, blank=True) # Many to one relationship

	def __str__(self):
		return self.name


'''Section for photo galerys'''

class Blueprint(models.Model):
	"""Model of Blueprint"""
	descr = models.CharField(max_length=1000)
	link = models.CharField(max_length=300)

	monument = models.ForeignKey(ArchitecturalMonument, related_name='blueprints') # One to one relationship
	
	def __str__(self):
		return self.descr

class NewPhoto(models.Model):
	"""Model of NewPhoto"""
	descr = models.CharField(max_length=1000)
	link = models.URLField(max_length=300)

	monument = models.ForeignKey('ArchitecturalMonument') # Many to one relationship
	
	def __str__(self):
		return self.descr

class OldPhoto(models.Model):
	"""Model of OldPhoto"""
	descr = models.CharField(max_length=1000)
	link = models.URLField(max_length=300)

	monument = models.ForeignKey('ArchitecturalMonument') # Many to one relationship
	
	def __str__(self):
		return self.descr

class Mosaic(models.Model):
	"""Model of Mosaic"""
	descr = models.CharField(max_length=1000)
	link = models.URLField(max_length=300)

	monument = models.ForeignKey('ArchitecturalMonument') # Many to one relationship
	
	def __str__(self):
		return self.descr

class ExternalDecoration(models.Model):
	"""Model of ExternalDecoration"""
	descr = models.CharField(max_length=1000)
	link = models.URLField(max_length=300)

	monument = models.ForeignKey('ArchitecturalMonument') # Many to one relationship
	
	def __str__(self):
		return self.descr


class FloorTile(models.Model):
	"""Model of FloorTile"""
	descr = models.CharField(max_length=1000)
	link = models.URLField(max_length=300)

	monument = models.ForeignKey('ArchitecturalMonument') # Many to one relationship
	
	def __str__(self):
		return self.descr


class Fresco(models.Model):
	"""Model of Fresco"""
	descr = models.CharField(max_length=1000)
	link = models.URLField(max_length=300)

	monument = models.ForeignKey('ArchitecturalMonument') # Many to one relationship
	
	def __str__(self):
		return self.descr


class Other(models.Model):
	"""Model of Other"""
	descr = models.CharField(max_length=1000)
	link = models.URLField(max_length=300)

	monument = models.ForeignKey('ArchitecturalMonument') # Many to one relationship
	
	def __str__(self):
		return self.descr


class Pirofillit(models.Model):
	"""Model of Pirofillit"""
	descr = models.CharField(max_length=1000)
	link = models.URLField(max_length=300)

	monument = models.ForeignKey('ArchitecturalMonument') # Many to one relationship
	
	def __str__(self):
		return self.descr


class WsDetail(models.Model):
	"""Model of WsDetail"""
	descr = models.CharField(max_length=1000)
	link = models.URLField(max_length=300)

	monument = models.ForeignKey('ArchitecturalMonument') # Many to one relationship
	
	def __str__(self):
		return self.descr


'''Other parts'''
class Book(models.Model):
	"""Model of Book"""
	publisher = models.CharField(max_length=100)
	author = models.CharField(max_length=100)
	descr = models.CharField(max_length=1000)

	monument = models.ForeignKey('ArchitecturalMonument') # Many to one relationship

	def __str__(self):
		return self.descr

class DetailedDescription(models.Model):
	"""Model of DetailedDescription"""
	descr = models.TextField()

	monument = models.ForeignKey('ArchitecturalMonument') # One to one relationship
	

class Reconstruction(models.Model):
	"""Model of Reconstruction"""
	author = models.CharField(max_length=100)
	descr = models.CharField(max_length=1000)
	year = models.CharField(max_length=4)

	century = models.ForeignKey('Century') # Many to one relationship
	monument = models.ForeignKey('ArchitecturalMonument') # Many to one relationship


class ResearchingHistory(models.Model):
	"""Model of ResearchingHistory"""
	author = models.CharField(max_length=100)
	descr = models.CharField(max_length=1000)
	years = models.CharField(max_length=100)

	monument = models.ForeignKey('ArchitecturalMonument') # One to one relationship
		
		
class GeoCoordinate(models.Model):
	"""Model of GeoCoordinate"""
	breadth = models.CharField(max_length=100)
	longitude = models.CharField(max_length=100)

	monument = models.ForeignKey('ArchitecturalMonument') # One to one relationship
		