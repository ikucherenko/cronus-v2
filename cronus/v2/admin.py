from django.contrib import admin
from .models import *

admin.site.register(ArchitecturalMonument)
admin.site.register(City)
admin.site.register(District)
admin.site.register(Region)
admin.site.register(Century)
admin.site.register(TypeOfMonument)
admin.site.register(Blueprint)