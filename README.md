Реализация бекенда проэкта Cronus на Python с применением Django REST + JWT Tokens.  


User Story - проекта министерства культуры. База данных по историко-культурным архитектурным памяткам Черниговщины. Перечень разделов и их описание со слов заказчика.

1. Страница авторизации - (логин, пароль)

2. Главная страница - На главной странице список памяток (в алфавитном порядке), на памятках есть миниатюра титульной фотографии (если фотография не задана то отображается миниатюра с надписью “немає фото”), название, дата (или век) строительства, и знак типа строения (типов строений всего три: Культовые, Светские, Производственные). Есть возможность поиска по названию памятки, по типу строения, дате строительства (тип date) и по веку (римские цифры), региону (пока что только Чернигов и Новгород сиверский), населенному пункту и району в населенном пункте, как в отдельности, так и в совокупности условий (так как опций поиска много оставить на виду только поиск по имени, остальные критерии сделать доступными при клике по ссылке “расширенный поиск”). При клике по памятке переходим на страницу самой памятки.

3. Страница памятки - Оформление страницы памятки: Титульная фотография (как строение выглядит на данный момент, если фотография не задана то отображается изображение с надписью “немає фото”), текст (краткое описание). На странице так же находятся ссылки на следующие разделы памятки:

4. Новые фото - содержит галерею с фотографиями современности, и кратким описанием по каждой фотографии.


5. Старые фото - аналогично разделу “Новые фото” только с фотографиями которые делались на протяжении всего изучения объекта.

6. Чертежи - раздел с чертежами, и кратким описанием чертежа.

7. История исследований - перечень исследований проводимых по объекту, краткое описание работ, с информацией об авторе исследования и годам исследования. (возможно нужно будет реализовывать выборку по автору исследования и году).

8. Реконструкция/Реставрация - Со списком данных: столетие (римские цифры), автор, описание проделанных работ - реализовать поиск в разделе по авторам и дате.

9. Библиография - сугубо текст, с указанием автора (реализовать выборку по автору)

10. Интерьер - Фотографии интерьера с описанием. + подразделы: Фреска-живопись (фото, текст), Пирофиллит (фото, текст), белокаменные детали (фото, текст), мозаика (фото, текст), плитка для пола (фото, текст).

11. Детальное описание - Сугубо текст, большого объема, со слов заказчика около 10 вордовских страниц. При добавлении/редактировании нового описания есть возможность простого форматирования текста (жирный, курсив, размер кегли).

12. Внешнее убранство - галерея с фотографиями и с кратким описанием для фото.

13. Геокоордината - GoogleMaps API Framework

14. Админка - управление пользователями и правами

Весь проект должен быть на украинском языке